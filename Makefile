ANIMATED_SOURCES=$(wildcard src/animated/*)
ANIMATED_TARGETS=$(patsubst src/animated/%,animated_out/%.typ,${ANIMATED_SOURCES})

animated_out/%.typ: src/animated/% Makefile
	@mkdir ${@D} -p
	@echo "Building animated slides for $<"
	codepresenter $< -o animated_out typst ./animated_prelude.typ

animated_all: ${ANIMATED_TARGETS}
