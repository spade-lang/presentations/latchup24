#import "typst-presentation-common/theme.typ": *
#import "typst-presentation-common/highlight.typ": *
#import "typst-presentation-common/animation.typ": *
#import "@preview/cades:0.3.0": qr-code

#show strong: set text(fill: rgb(140, 230, 255), weight: 400)
// #show raw.where(lang: none): set text(fill: rgb(251, 200, 150))

#show: university-theme.with(
  progress-bar: false
)

#show: highlights


// #show raw.where(lang: "spade"): it => [
//   #show regex("\b(entity|let|inst|end)\b") : keyword => text(weight:"bold", keyword)
//   #it
// ]

#title-slide(
  title: [#box(image("spadefish_outline.svg", height: 1em)) Spade - An HDL Inspired by Modern Software Languages],
  authors: "Frans Skarman",
  institution-name: "Linköping University",
  logo: image("liu_logo.svg", height: 3cm),
)


#slide(title: "")[
  #only("1", image("./fig/languages_sw_dots.svg", height:100%))
  // #only("2", image("./fig/languages_sw_names.svg", height:100%))
  #only("2", image("./fig/languages_sw_dots.svg", height:100%))
  #only("3", image("./fig/languages_hw_dots.svg", height:100%))
  #only("4", image("./fig/languages_hw_names.svg", height:100%))
]

#slide(title: "Stealing From Software")[
  #box(height: 100%, width: 100%)[
    #reveal("1", [ - *Fearless* refactoring ])
    #reveal("2", [ - *Abstractions* for hardware])
    #reveal("3", [ - *Low* performance overhead])
    #reveal("4|", [ - Great *tooling* ])

    #place(bottom + right, image("./fig/spadethief.svg", width: 5cm))
  ]
]

#slide(title: "Pipelines")[
  #grid(columns: (60%, 40%), [
      #set align(horizon)
      #set text(size: 18pt)
      #include "./animated_out/pipeline.typ"

      #only("6", [
        ```error
        @rerrorr@: Use of x before it is ready
           @b-->b@ src/main.spade:10:19
           @b|b@
        10 @b|b@ let sum = x + f(a, product);
           @b|b@           @r^ Is unavailable for another stager@
           @b=b@ Requesting x from stage 1
           @b=b@ But it will not be available until stage 2
        ```
      ])
    ], [
      #set align(center)
      #only("1-2", image("./fig/pipeline_part1.svg", height: 100%))
      #only("3-4", image("./fig/pipeline_part2.svg", height: 100%))
      #only("5-6", image("./fig/pipeline_part3.svg", height: 100%))
      #only("7-8", image("./fig/pipeline_part4.svg", height: 100%))
    ]
  )
]


#slide(title: [#only("1", [Demo*?*]) #only("2-", [Demo *No `:(`*])])[
  #set align(center)
  #only("2", image("./fig/plane_setup.jpg", height: 100%))
  #only("3", image("./fig/plane_wing.jpg", height: 100%))
]

#slide(title: "CSI2")[
  #v(2cm)
  #line-by-line([
    Camera protocol
    #reveal("2")[- 1-4 lanes + clock]
    #reveal("3")[- Find SOT]
    #reveal("4")[- Merge lanes ]
    #reveal("5")[- Look for packet headers]
    #reveal("6")[- Separate short and long packets ]
    #reveal("7|")[- Pixels after packet with header `0x2A`]
  ])
]
#slide(title: "Streams")[
  #grid(columns: (32%, 68%), gutter: 1em, [
    #set align(horizon)

    #hl("2")[- 1-4 lanes + clock]
    #hl("3")[- Find SOT]
    #hl("4")[- Merge lanes ]
    #hl("5")[- Look for packet headers]
    #hl("6")[- Separate short and long packets ]
    #hl("7|")[- Pixels after packet with header `0x2A`]
    ], [
      #include "./animated_out/csi.typ"
    ]
  )
]


#slide(title: "Streams")[
  #set text(size: 20pt)

  #include "./animated_out/pixel_stream.typ"
]


#focus-slide()[
  #set align(center + horizon)
 #box(width: 100%, height: 100%, [
    A language is nothing without its *tools*


    #place(bottom + right, [#v(1cm) #image("./fig/spadefish-spade.svg", width: 4cm)])
  ])
]

#slide(title: "Swim - The Spade Build Tool")[
  #box(height: 100%)[
    #set align(horizon)
    #set text(size: 20pt)
    #grid(columns: (60%, 40%), [
        #include "./animated_out/config.typ"
      ], [
        #only(2, [
          ```shell_commands
          swim plugin litedram_setup
          swim plugin litedram_generate
          ```

          // #image("./fig/flamegraph.png")
        ])
      ]
    )
  ]
]

#slide(title: "Swim - The Spade Build Tool")[
  #set align(horizon)
  #box(height: 100%)[
    Easy setup

    ```shell_commands
    cargo install swim
    swim install-tools
    git clone "git:gitlab.com/user/project"

    swim upload
    ```
  ]
]


#slide(title: "Conclusion")[
  #box(height: 100%, width: 100%)[
    #list(
      [#hl("1-3,10|")[*Fearless* refactoring]
        #reveal("3,10|")[
          #list(
            [*Pipelines* for trivial re-timing],
            [Strong type systems that *catches bugs early* ]
          )
        ]
      ],
      [#hl("1,4-5,10|")[*Abstractions* for hardware]
        #reveal("5,10|")[
          #list(
            [*Pipelines*],
            [*Memories and ports* as primitives],
            [*Stream interfaces* expressible in the language],
          )
        ]
      ],
      [#hl("1,6-7,10|")[*Low* performance overhead]
        #reveal("7,10|")[
          #list(
            [*RTL* level description]
          )
        ]
      ],
      [#hl("1,8-9,10|")[*Tooling*]
        #reveal("9,10|")[
          #list(
            [*Helpful* compiler],
            [Powerful *build system*],
            [Purpose built *waveform viewer*],
          )
        ]
      ]
    )


    #place(
      top + right,
      stack(dir: ttb,
        only("10", box[
          #set align (center)
          #set text(size: 26pt)
          #qr-code("https://spade-lang.org/latchup24", width: 4cm)
          #link("https://spade-lang.org")[*`spade-lang.org`*]

          #set text(size: 22pt)
          #link("https://mastodon.social/@thezoq2")[`mastodon.social/`*`@thezoq2`*]

          #link("mailto:frans.skarman@liu.se")[`frans.skarman@liu.se`]
        ]),
        v(1fr),
        stack(dir: ltr, [
          #only("10", image("./liu_logo.svg", width: 6cm))
          ],
          h(2cm),
          [
            #only("1-9", image("./fig/spadethief.svg", width: 5cm))
            #only("10", image("./fig/lolspade.svg", width: 5cm))
          ]
        )
      )
    )
  ]
]


